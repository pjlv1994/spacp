import { Component, OnInit } from '@angular/core';
import * as icon from '@fortawesome/free-solid-svg-icons';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.sass']
})
export class ScheduleComponent implements OnInit {
  user=icon.faUser;
  calendarCheck = icon.faCalendarCheck
  userPlus = icon.faUserPlus;
  userTimes = icon.faUserTimes;
  search = icon.faSearch;
  CheckSquare = icon.faCheckSquare
  longArrowright = icon.faLongArrowAltRight
  times = icon.faTimes
  equals = icon.faEquals
  check = icon.faCheck
  searchActive: boolean = false;
  nombre: string = '';
  nombre2: string = '';
  medicoNombre: string = '';
  cedula;
  tratamiento;
  doctor: string = '';
  monto;
  modalID;
  delete2: boolean = false;
  constructor(private modalService: NgbModal) { }
  open(content,id) {
    this.modalService.open(content, { size: 'lg' });
    this.modalID = id;
    if(this.modalID == 2){
      this.delete2 = true;
    }
  }
  ngOnInit() {
  }
  Search(){
    if(this.cedula == undefined){
      console.log('CAMPO VACIO');
    }
    else {    
      this.searchActive = true;
      this.nombre = 'Ana Rojas [P-01]';
      this.nombre2 = 'Maria Granado [P-03]';
      this.tratamiento = 'Inyecciones de botox'
    }
  }
  delete(){
    this.searchActive = false;
    this.nombre = '';
    this.monto;
    this.medicoNombre = '';
    this.tratamiento = '';

  }
  status(){
    if( this.tratamiento <= 5){
      this.medicoNombre = 'Rafael Medina [M-01]';
      this.monto = 1000000;
    }
    if( this.tratamiento >= 6){
      this.medicoNombre = 'Ana Hurtado [M-02]';
      this.monto = 4000000;
    }
    if( this.tratamiento >= 10 && this.tratamiento <18 ){
      this.medicoNombre = 'Merida Rodriguez [M-03]';
      this.monto = 8000000;
    }

  }
}
