import { Component, OnInit,ChangeDetectorRef, OnDestroy } from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import * as icon from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router'

@Component({
  selector: 'app-navbar-dashboard',
  templateUrl: './navbar-dashboard.component.html',
  styleUrls: ['./navbar-dashboard.component.sass']
})
export class NavbarDashboardComponent implements OnInit {
  showFiller: boolean = true; 

  constructor(private router: Router) {}

  textBtn : string = 'Cerrar sesión';
  loading: boolean = false;
  user=icon.faUsers;
  userMd=icon.faUserMd;

  iconLoading = icon.faSpinner;
  bars = icon.faBars;
  tachometerAlt = icon.faTachometerAlt;
  cashRegister = icon.faCashRegister;
  longArrowLeft = icon.faLongArrowAltLeft;
  calendar = icon.faCalendar
  idMenu = 0;

  ngOnInit() {

  }
  closet(){
    this.loading = true;
    this.router.navigate(['/login']);
  }
  mostrar(){
    let sidebar = document.querySelector('#sidebar')
    sidebar.classList.toggle('active')
  }
  menu(idMenu){
    this.idMenu = idMenu;
  }

}

