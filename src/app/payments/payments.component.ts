import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as icon from '@fortawesome/free-solid-svg-icons';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.sass']
})
export class PaymentsComponent implements OnInit {
  CheckSquare = icon.faCheckSquare
  longArrowright = icon.faLongArrowAltRight
  times = icon.faTimes
  equals = icon.faEquals
  check = icon.faCheck
  fileDollar = icon.faFileInvoiceDollar;

  referenceInput = 802180239130;
  referenceInput2 = null;
  referenceActive: boolean = false;
  amountInput = 2500100;
  amountInput2;
  amountActive: boolean = false;
  meansInput = 'Transferencia (Mercantil)';
  meansInput2 = null;
  meansActive: boolean = false;
  pago;
  greenReference: boolean = false;
  orangeReference: boolean = false;

  greenAmount: boolean = false;
  orangeAmount: boolean = false;

  greenMeans: boolean = false;
  orangeMeans: boolean = false;

  referenceAccept: boolean = false;
  amountAccept: boolean = false;
  meansAccept: boolean = false;
  modalID;
  data = [];


  
  closeResult = '';

  slides = [
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 1",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 2",
    },    
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 3",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 4",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 5",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 6",
    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 7",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 8",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 9",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 10",

    },
    {
      img: "https://static.vecteezy.com/system/resources/previews/000/377/805/non_2x/wifi-vector-icon.jpg",
      contract: "Pago 11",

    }

  ];
  slideConfig = {
    "slidesToShow": 8, "slidesToScroll": 5, 
    'responsive': [
      {
        'breakpoint': 767,
         'settings': {
          'slidesToShow': 3
                }
              }
            ]
};
  constructor(private modalService: NgbModal) { }
  open(content,id) {
    this.modalService.open(content, { size: 'lg' });
    this.modalID = id;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  ngOnInit() {}

  reference(item){
    if(item == 1) {
      this.referenceActive = true;
      this.greenReference = true;
      return this.referenceInput2 = this.referenceInput;
    }
    if(item == 2){
      this.referenceActive = false;
      this.greenReference = false;
      this.orangeReference = false;
      this.referenceAccept = false;
      return this.referenceInput2 = null;
    }
    if(item == 3){
      if (this.referenceInput2 == this.referenceInput) {
        this.referenceAccept = true;
      }
      if (this.referenceInput2 != this.referenceInput && this.referenceInput2 != null) {
        this.orangeReference = true;
        this.referenceActive = true;
        this.referenceAccept = true;
        if(this.greenReference == true)
          this.greenReference = false;
      }
    }
  }

  amount(item){
    if(item == 1) {
      this.amountActive = true;
      this.greenAmount = true;
      return this.amountInput2 = this.amountInput;
    }
    if(item == 2) {
      this.amountActive = false;
      this.greenAmount = false;
      this.orangeAmount = false;
      this.amountAccept = false;
      return this.amountInput2 = null;
    }
    if(item == 3) {
      if (this.amountInput2 == this.amountInput) {
        this.amountAccept = true;
      }
      if (this.amountInput2 != this.amountInput && this.amountInput2 != null) {
        this.orangeAmount = true;
        this.amountActive = true;
        this.amountAccept = true;
        if(this.greenAmount == true)
          this.greenAmount = false;
      }
    }
    
  }
  credito(){
    console.log('test');
  }
  means(item){
    if(item == 1){
      this.meansActive = true;
      this.greenMeans = true;
      return this.meansInput2 = this.meansInput;
    }
    if(item == 2){
      this.meansActive = false;
      this.greenMeans = false;
      this.orangeMeans = false;
      this.meansAccept = false;
      return this.meansInput2 = null;
    }
    if(item == 3) {
      if (this.meansInput2 == this.meansInput) {
        this.meansAccept = true;
      }
      if (this.meansInput2 != this.meansInput && this.meansInput2 != null) {
        this.orangeMeans = true;
        this.meansActive = true;
        this.meansAccept = true;
        if(this.greenMeans == true)
          this.greenMeans = false;
      }
    }
  }
}
