import { Component, OnInit } from '@angular/core';
import * as icon from '@fortawesome/free-solid-svg-icons';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.sass']
})
export class DoctorComponent implements OnInit {
  search = icon.faSearch;
  check = icon.faCheck
  times = icon.faUserTimes;
  fileDollar = icon.faFileInvoiceDollar;
  notesMedical = icon.faNotesMedical;
  user = icon.faUserMd;
  delete = icon.faTrashAlt;
  addUser = icon.faUserPlus;
  CheckSquare = icon.faCheckSquare
  modalID;
  codigo = 'M-02';
  cedula = 12345678;
  telefono: string = '0294-3310001'
  especialidad: string = '0294-3310001'
  nombre: string = 'Rafael';
  apellido: string = 'Medina';

  tratante: string = 'Ana Hurtado [M-02]';
  deleteUser: boolean = false


  constructor(private modalService: NgbModal) { }

  ngOnInit() {}
  open(content,id) {
    this.modalService.open(content, { size: 'lg' });
    this.modalID = id;
  }
  DeleteUser(){
    return this.deleteUser = true;
  }
}
