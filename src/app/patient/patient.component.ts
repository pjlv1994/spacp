import { Component, OnInit } from '@angular/core';
import * as icon from '@fortawesome/free-solid-svg-icons';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.sass']
})
export class PatientComponent implements OnInit {
  search = icon.faSearch;
  check = icon.faCheck;

  fileDollar = icon.faFileInvoiceDollar;
  notesMedical = icon.faNotesMedical;
  user = icon.faUser;
  times = icon.faTimes;
  delete = icon.faTrashAlt;
  addUser = icon.faUserPlus;
  CheckSquare = icon.faCheckSquare
  modalID;
  codigo = 'P-02';
  cedula = 12345678;
  telefono: string = '0294-3310001'
  nombre: string = 'Ana';
  apellido: string = 'Rojas';
  historial: string = 'Alérgica al ibuprofeno';
  observaciones: string = 'Acné';
  tratante: string = 'Ana Hurtado [M-02]';
  deleteUser: boolean = false


  constructor(private modalService: NgbModal) { }

  ngOnInit() {}
  open(content,id) {
    this.modalService.open(content, { size: 'lg' });
    this.modalID = id;
  }
  DeleteUser(){
    return this.deleteUser = true;
  }

}
