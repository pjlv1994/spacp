import { Component, OnInit, NgModule } from '@angular/core';
import * as icon from '@fortawesome/free-solid-svg-icons';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  active : boolean = false;
  active2 : boolean = false;
  active3 : boolean = false;


  icon : any;
  lock = icon.faLock
  longArrowRight = icon.faLongArrowAltRight
  iconLoading = icon.faSpinner;
  textForm : string = 'Ingresar';
  textFormVal = '';

  loading: boolean = false;
  errorBoolMsg: boolean = false;

  errorMsg: string = '';

  email = null;
  password = null;
  data = [];

  constructor (private router: Router) { }

  ngOnInit() {
    this.icon = this.longArrowRight;
  }
  ngOnSubmit(){
    this.loading = true;
    this.textFormVal = 'Ingresando';
    this.icon = this.iconLoading
    if (this.email === 'admin@spa.com' && this.password === 'admin') {
      this.data.push({
        Email: this.email,
        Password: this.password
      });
      console.log("Obj-Login",this.data[0])
      this.router.navigate(['/home'])


    }
    if(this.email == 'admin@spa.com' && this.password != null){
      this.errorBoolMsg = true;
      this.errorMsg = 'La contraseña que ingresaste es incorrecta.';
      this.loading = false;
      this.textFormVal = this.textForm;
      this.icon = this.longArrowRight;
    }
    if(this.email != 'admin@spa.com'){
      this.errorBoolMsg = true;
      this.errorMsg = 'El correo electrónico no coincide con ninguna cuenta.';
      this.loading = false;
      this.textFormVal = this.textForm;
      this.icon = this.longArrowRight;
    }
    if(this.email == null || this.password == null || this.email == '' || this.password == '' ) {
      this.errorBoolMsg = true;
      this.errorMsg = 'Todos los campos son requeridos.';
      this.loading = false;
      this.textFormVal = this.textForm;
      this.icon = this.longArrowRight;
    }
  }
  Active(){
    this.active = true;
  }
  Active2(){
    this.active2 = true;
  }
  Active3(){
    this.active3 = true;
  }
}

  



